<?php

namespace App\Console;

use App\Helpers\Instagram;
use App\Models\Post;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $posts = \DB::table('posts')
                ->join('social_media_accounts', 'social_media_accounts.id', 'posts.social_media_account_id')
                ->where('status', 'new')
                ->whereNotNull('username')
                ->where('photo_filename', '!=', '')
                ->select('posts.id', 'social_media_accounts.username', 'social_media_accounts.password',
                    'posts.post_body', 'posts.photo_filename', 'posts.time_to_post')
                ->get();

            foreach ($posts as $post) {
                $dateNow = date('Y-m-d H:i:s');
                if (strtotime($dateNow) <= strtotime($post->time_to_post)) {
                    $instagram = new Instagram($post->username, $post->password);
                    $postData = $instagram->postMedia($post->post_body, $post->photo_filename);

                    $postId = collect($postData->getMedia())->all()->id;

                    $updatePost=Post::find($post->id);
                    $updatePost->status='success';
                    $updatePost->post_id=$postId;
                    $updatePost->save();
                }
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
