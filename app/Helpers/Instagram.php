<?php
namespace App\Helpers;

use InstagramAPI\Utils;

class Instagram
{
    private $username;
    private $password;
    private $instagramInstance;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        $debug = false;
        $truncatedDebug = false;

        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;

        $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);
        $ig->login($username, $password);

        $this->instagramInstance = $ig;
    }

    /**
     * Uploads a photo to your Instagram timeline.
     *
     * @param string $body media comment
     * @param array  $picturePath The photo filename.
     *
     * @return \InstagramAPI\Response\ConfigureResponse
     *
     */
    public function postMedia(string $body, string $picturePath)
    {
        $photo = new \InstagramAPI\Media\Photo\InstagramPhoto(storage_path().'/app/'.$picturePath);
        return $this->instagramInstance->timeline->uploadPhoto($photo->getFile(), ['caption' => $body]);
    }

    public function getPostData(string $postId)
    {
        return $this->instagramInstance->media->getInfo($postId);
    }

    /**
     * Post a comment on a media item.
     *
     * @param string      $mediaId        The media ID in Instagram's internal format (ie "3482384834_43294").
     * @param string      $commentText    Your comment text.
     * @param string|null $replyCommentId (optional) The comment ID you are replying to, if this is a reply (ie "17895795823020906");
     *                                    when replying, your $commentText MUST contain an @-mention at the start (ie "@theirusername Hello!").
     * @param string      $module         (optional) From which app module (page) you're performing this action.
     *                                    "comments_v2" - In App: clicking on comments button,
     *                                    "self_comments_v2" - In App: commenting on your own post,
     *                                    "comments_v2_feed_timeline" - Unknown,
     *                                    "comments_v2_feed_contextual_hashtag" - Unknown,
     *                                    "comments_v2_photo_view_profile" - Unknown,
     *                                    "comments_v2_video_view_profile" - Unknown,
     *                                    "comments_v2_media_view_profile" - Unknown,
     *                                    "comments_v2_feed_contextual_location" - Unknown,
     *                                    "modal_comment_composer_feed_timeline" - In App: clicking on prompt from timeline.
     *
     * @throws \InvalidArgumentException
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return \InstagramAPI\Response\CommentResponse
     */
    public function writeComment(string $postId, string $comment, ?string $replyCommentId = null)
    {
        return $this->instagramInstance->media->comment($postId, $comment, $replyCommentId);
    }

    /**
     * Get media comments.
     *
     * Note that this endpoint supports both backwards and forwards pagination.
     * The only one you should really care about is "max_id" for backwards
     * ("load older comments") pagination in normal cases. By default, if no
     * parameter is provided, Instagram gives you the latest page of comments
     * and then paginates backwards via the "max_id" parameter (and the correct
     * value for it is the "next_max_id" in the response).
     *
     * However, if you come to the comments "from a Push notification" (uses the
     * "target_comment_id" parameter), then the response will ALSO contain a
     * "next_min_id" value. In that case, you can get newer comments (than the
     * target comment) by using THAT value and the "min_id" parameter instead.
     *
     * @param string $mediaId The media ID in Instagram's internal format (ie "3482384834_43294").
     * @param array  $options An associative array of optional parameters, including:
     *                        "max_id" - next "maximum ID" (get older comments, before this ID), used for backwards pagination;
     *                        "min_id" - next "minimum ID" (get newer comments, after this ID), used for forwards pagination;
     *                        "target_comment_id" - used by comment Push notifications to retrieve the page with the specific comment.
     *
     * @throws \InvalidArgumentException
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return \InstagramAPI\Response\MediaCommentsResponse
     */
    public function getComments(string $postId, array $options = [])
    {
        return $this->instagramInstance->media->getComments($postId, $options);
    }

    /**
     * Get the replies to a specific media comment.
     *
     * You should be sure that the comment actually HAS more replies before
     * calling this endpoint! In that case, the comment itself will have a
     * non-zero "child comment count" value, as well as some "preview comments".
     *
     * If the number of preview comments doesn't match the full "child comments"
     * count, then you are ready to call this endpoint to retrieve the rest of
     * them. Do NOT call it frivolously for comments that have no child comments
     * or where you already have all of them via the child comment previews!
     *
     * @param string $mediaId   The media ID in Instagram's internal format (ie "3482384834_43294").
     * @param string $commentId The parent comment's ID.
     * @param array  $options   An associative array of optional parameters, including:
     *                          "max_id" - next "maximum ID" (get older comments, before this ID), used for backwards pagination;
     *                          "min_id" - next "minimum ID" (get newer comments, after this ID), used for forwards pagination.
     *
     * @throws \InvalidArgumentException
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return \InstagramAPI\Response\MediaCommentRepliesResponse
     */
    public function getCommentReplies(string $postId, string $commentId, array $options = [])
    {
        return $this->instagramInstance->media->getCommentReplies($postId, $commentId, $options);
    }

    /**
     * Delete a comment.
     *
     * @param string $mediaId   The media ID in Instagram's internal format (ie "3482384834_43294").
     * @param string $commentId The comment's ID.
     *
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return \InstagramAPI\Response\DeleteCommentResponse
     */
    public function deleteComment(string $uploadId, string $commentId)
    {
        return $this->instagramInstance->media->deleteComment($uploadId, $commentId);
    }

    /**
     * Delete a media item.
     *
     * @param string     $mediaId   The media ID in Instagram's internal format (ie "3482384834_43294").
     * @param string|int $mediaType The type of the media item you are deleting. One of: "PHOTO", "VIDEO"
     *                              "CAROUSEL", or the raw value of the Item's "getMediaType()" function.
     *
     * @throws \InvalidArgumentException
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return \InstagramAPI\Response\MediaDeleteResponse
     */
    public function deletePost( string $mediaId, $mediaType = 'PHOTO')
    {
        return $this->instagramInstance->media->delete($mediaId);
    }

    public function getUserPosts($username)
    {
        $userId = $this->instagramInstance->people->getUserIdForName($username);
        return $this->instagramInstance->timeline->getUserFeed($userId);
    }
}
