<?php
namespace App\Helpers;

use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT as FirebaseJWT;

/**
 * Jwt class to check and generate jwt token to use for login and auth.
 */
class Jwt
{

    /**
     * The expire seconds for token created
     */
    const EXPIRE_AFTER = 3600;
    private $data;

    public function __construct()
    {
        //The initial data of the token
        $this->data = [
            'id' => '',
            'iss' => "avokado", // Issuer of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + self::EXPIRE_AFTER, // Expiration time
        ];
    }

    /**
     * Generate new token
     * @param int $userId
     * @return string
     */
    public function get(int $userId)
    {
        $this->data['id'] = $userId;
        return FirebaseJWT::encode($this->data, env('JWT_SECRET'));
    }

    /**
     * Check token and return data
     * @param string $token
     * @return array
     */
    public function check(string $token)
    {
        try {
            return ['success', FirebaseJWT::decode($token, env('JWT_SECRET'), ['HS256'])];
        } catch (ExpiredException $e) {
            FirebaseJWT::$leeway = 720000;
            $decoded = (array) FirebaseJWT::decode($token, env('JWT_SECRET'), ['HS256']);
            $decoded['iat'] = time();
            $decoded['exp'] = time() + self::EXPIRE_AFTER;
            return ['token_expired', ['token' => FirebaseJWT::encode($decoded, env('JWT_SECRET'))]];
        } catch (\Exception $e) {
            return ['token_invalid', null];
        }

    }
}
