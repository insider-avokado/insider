<?php

namespace App\Http\Controllers;

use App\Repositories\AnalyticRepository;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    /**
     * Get list of post analytics
     * @params $request
     * @return Response
     */
    public function get(Request $request)
    {
        $postId = $request->post_id;
        $brandsRepo = new AnalyticRepository();
        $list = $brandsRepo->getList($postId);
        return response()->json($list, 200);
    }
}
