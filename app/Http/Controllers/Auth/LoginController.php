<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Jwt;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Repositories\AgencyRepository;
use App\Repositories\BrandRepository;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login using username or email and password
     * @param LoginRequest
     * @return response
     */

    public function login(LoginRequest $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $user = User::with(['agency'])
            ->where(function ($query) use ($username) {
                $query->where('username', $username)
                    ->orWhere('email', $username);
            })
            ->where('is_active', 'yes')
            ->first();

        if (!$user) {
            return response('Unauthorized', 401);
        }

        if (!\Hash::check($password, $user->password)) {
            return response('Unauthorized', 401);
        }

        $jwt = new Jwt();
        $token = $jwt->get($user->id);
        $responseData = [
            'user' => $user->only('id', 'name', 'user_type'),
            'token' => $token,
        ];

        $agencyRepo = new AgencyRepository();
        $brandRepo = new BrandRepository();
        if ($user->user_type == 'super') {
            $responseData['agencies'] = $agencyRepo->getList();
        } elseif ($user->user_type == 'agency') {
            $responseData['clients'] = $brandRepo->getList($user->agency->id);
        }
        return response()->json($responseData);
    }
}
