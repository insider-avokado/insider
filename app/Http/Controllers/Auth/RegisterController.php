<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Jwt;
use App\Http\Controllers\Controller;
use App\Repositories\AgencyRepository;
use App\Repositories\UserRepository;
use App\User;
use App\Http\Requests\UserRequest;

class RegisterController extends Controller
{

    /**
     * Register new user
     * @param RegisterRequest $request
     * @return Response
     */
    public function register(UserRequest $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $name = $request->input('name');
        $email = $request->input('email');
        $userRepo = new UserRepository();
        $agencyRepo = new AgencyRepository();

        //create agency user first
        $user = $userRepo->create([
            'username' => $username,
            'name' => $name,
            'password' => \Hash::make($password),
            'email' => $email,
        ]);
        //check if user couldn't be saved
        if (!$user) {
            return response('Cannot create user, please try again', 500);
        }

        //now create agency for the user
        $agency = $agencyRepo->create([
            'name' => $name,
            'user_id' => $user->id,
        ]);

        //if the agency couldn't be saved we need to delete the user itself
        if (!$agency) {
            $userRepo->delete($user->id);
            return response('Cannot create agency, please try again', 500);
        }
        //create token to be login after register immidiatly
        $jwt = new Jwt();
        $token = $jwt->get($user->id);
        return response()->json(['user' => $user, 'token' => $token, 'clients' => []]);
    }
}
