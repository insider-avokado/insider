<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandRequest;
use App\Http\Requests\CheckHashRequest;
use App\Mail\BrandAccountsLink;
use App\Models\Brand;
use App\Repositories\BrandRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use \App\User;

class BrandsController extends Controller
{

    /**
     * Get list of user brands
     * @params $request
     * @return Response
     */
    public function get(Request $request)
    {
        $agency = $request->user->agency;
        $brandsRepo = new BrandRepository();
        $list = $brandsRepo->getList($agency->id);
        return response()->json($list, 200);
    }

    /**
     * Create new brand
     * @params $request
     * @return Response
     */
    public function create(BrandRequest $request)
    {
        $userId = $request->user->id;
        $checkCanCreate = $this->checkSubscription($userId);
        if (!$checkCanCreate[0]) {
            return response()->json(['message' => $checkCanCreate[1]], 403);
        }
        $agency = $request->user->agency;
        $name = $request->input('name');
        $email = $request->input('email');
        $hash = \Hash::make($email);
        $brandRepo = new BrandRepository();
        $create = $brandRepo->create([
            'name' => $name,
            'email' => $email,
            'hash' => $hash,
            'agency_id' => $agency->id,
        ]);
        if ($create) {
            $create->form_url = env('WEBSITE_URL') . '/clients/accounts?hash=' . $hash;
            Mail::to($create->email)->send(new BrandAccountsLink($create));
            return response('Success', 200);
        }
        return response()->json(['message' => 'Error in creating new brand, please try again later'], 500);
    }

    /**
     * Check for brand hash
     * @params $request
     * @return Response
     */
    public function checkHash(CheckHashRequest $request)
    {
        $brandRepo = new BrandRepository();

        $brand = $brandRepo->getByHash($request->input('hash'));
        if (!$brand) {
            return response('Not Found', 404);
        }

        return response('Success', 200);

    }

    /**
     * Check if user subscribed and check if the user can create new brand
     * @param $userId
     * @return array
     * */

    public function checkSubscription(int $userId): array
    {
        $user = User::find($userId);
        $sub = $user->subscriptions;
        $brandsCount = Brand::whereHas('agency.user', function ($query) use ($userId) {
            $query->where('users.id', $userId);
        })->count();

        if ($sub == null) {
            if ($brandsCount > 1) {
                return [false, 'You cannot create more Brands, Please subscribe to one of our plans'];
            }
        } else {
            if ($user->subscriptions()->first()->subscription_type == 'professional') {
                if ($brandsCount > 3) {
                    return [false, 'You cannot create more Brands, Please Choose premium plan'];
                }
            }
            if ($user->subscriptions()->first()->subscription_type == 'premium') {
                if ($brandsCount > 5) {
                    return [false, 'You cannot create more Brands, Please Call us', 403];
                }
            }
        }
        return [true];

    }

}
