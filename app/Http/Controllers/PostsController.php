<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentWriteRequest;
use App\Http\Requests\DestroyCommentRequest;
use App\Http\Requests\DestroyPostRequest;
use App\Http\Requests\IndexCommentReplyRequest;
use App\Http\Requests\IndexCommentRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Helpers\Instagram;
use App\Http\Requests\PostWriteRequest;

class PostsController extends Controller
{
    /**
     * Get user posts
     * @param Request
     * @return Response
     */
    public function posts(Request $request, $id) {
        $userId = $request->user->id;

        try {
            $user = \DB::table('users')
                ->join('agencies', 'users.id', '=', 'agencies.user_id')
                ->join('brands', 'brands.agency_id', '=', 'agencies.id')
                ->join('social_media_accounts', 'social_media_accounts.brand_id', '=', 'brands.id')
                ->join('posts', 'posts.social_media_account_id', '=', 'social_media_accounts.id')
                ->where('user_id', $userId)
                ->where('social_media_accounts.id', $id)
                ->select('social_media_accounts.username', 'social_media_accounts.password')
                ->first();

            if ($user) {
                try {
                    $instagram = new Instagram($user->username, $user->password);
                    $postData = $instagram->getUserPosts($user->username);

                    return response()->json($postData, 200);

                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()], 500);
                }
            }
            else {
                return response("Forbidden", 403);
            }
        }
        catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get user posts with post id
     * @param Request, $id
     * @return Response
     */
    public function getOne (Request $request, $id) {
        $userId = $request->user->id;

        try {
            $user = \DB::table('users')
                ->join('agencies', 'users.id', '=', 'agencies.user_id')
                ->join('brands', 'brands.agency_id', '=', 'agencies.id')
                ->join('social_media_accounts', 'social_media_accounts.brand_id', '=', 'brands.id')
                ->join('posts', 'posts.social_media_account_id', '=', 'social_media_accounts.id')
                ->where('user_id', $userId)
                ->where('post_id', $id)
                ->select('social_media_accounts.username', 'social_media_accounts.password')
                ->first();

            if ($user) {
                try {
                    $instagram = new Instagram($user->username, $user->password);
                    $postData = $instagram->getPostData($id);

                    return response()->json($postData, 200);

                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()], 500);
                }
            }
            else {
                return response("Forbidden", 403);
            }
        }
        catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * post a new media to instagram
     * @param PostWriteRequest
     * @return Response
     */
    public function postMedia (PostWriteRequest $request) {

        $userId = $request->user->id;
        $socialMediaAccountId = $request->social_media_account_id;
        $postBody = $request->body ? $request->body : '';
        $postImage = $request->file('file');
        $timetoPost = $request->time_to_post;
        $dateToPost = $request->date_to_post;
        try {
            $user = \DB::table('users')
                ->join('agencies', 'users.id', '=', 'agencies.user_id')
                ->join('brands', 'brands.agency_id', '=', 'agencies.id')
                ->join('social_media_accounts', 'social_media_accounts.brand_id', '=', 'brands.id')
               // ->join('posts', 'posts.social_media_account_id', '=', 'social_media_accounts.id')
                ->where('users.id', $userId)
              //  ->where('social_media_account_id', $socialMediaAccountId)
                ->select('social_media_accounts.username', 'social_media_accounts.password', 'social_media_accounts.id')
                ->first();

            $dateNow = date('Y-m-d H:i:s');
            $ext = $postImage->getClientOriginalExtension();
            $photoName = sha1($postImage->getClientOriginalName().''.$dateNow).'.'.$ext;


            $postImage->move(storage_path().'/app/', $photoName);

            $scheculedTime = $dateToPost.' '.$timetoPost;

            $post = new Post;
            $post->social_media_account_id = $user->id;
            $post->post_body = $postBody;
            $post->photo_filename = $photoName;
            $post->time_to_post = $scheculedTime;
            $post->status = 'new';
            $post->save();

            if (strtotime($dateNow) >= strtotime($scheculedTime)) {
                try {
                    $instagram = new Instagram($user->username, $user->password);
                    $instagram = new Instagram($user->username, $user->password);
                    $postData = $instagram->postMedia($postBody, $photoName);

                    $postId = collect($postData->getMedia())->all()->id;

                    $updatePost = Post::find($post->id);
                    $updatePost->status = 'success';
                    $updatePost->post_id = $postId;
                    $updatePost->save();

                    return response("Post sended", 200);

                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()], 500);
                }
            }
            else {
                return response("Post scheduled", 200);
            }
        }
        catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * post a comment and reply comment to media
     * @param CommentWriteRequest
     * @return Response
     */
    public function writeComment (CommentWriteRequest $request) {

        $userId = $request->user->id;
        $postId = $request->post_id;
        $comment = $request->comment;
        $replyCommentId = $request->reply_comment_id;
        $user = $this->getUserWithPostId($userId, $postId);

        try {
            $instagram = new Instagram($user->username, $user->password);
            $comment = $instagram->writeComment($user->post_id, $comment, $replyCommentId);

            return response()->json("Comment sended", 200);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all comments with post id
     * @param IndexCommentRequest
     * @return Response
     */
    public function getComments (IndexCommentRequest $request, $postId) {

        $userId = $request->user->id;

        $user = $this->getUserWithPostId($userId, $postId);

        try {
            $instagram = new Instagram($user->username, $user->password);
            $items = $instagram->getComments($user->post_id);

            return response()->json($items, 200);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all comment replies with post id
     * @param IndexCommentReplyRequest
     * @return Response
     */
    public function getCommentReplies (IndexCommentReplyRequest $request, $postId) {

        $userId = $request->user->id;
        $postId = $request->post_id;
        $commentId = $request->comment_id;

        $user = $this->getUserWithPostId($userId, $postId);

        try {
            $instagram = new Instagram($user->username, $user->password);
            $items = $instagram->getCommentReplies($user->post_id, $commentId);

            return response()->json($items, 200);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Delete comment
     * @param DestroyCommentRequest
     * @return Response
     */
    public function deleteComment (DestroyCommentRequest $request, $postId, $commentId) {
        $userId = $request->user->id;
        $user = $this->getUserWithPostId($userId, $postId);

        try {
            $instagram = new Instagram($user->username, $user->password);
            $instagram->deleteComment($user->post_id, $commentId);

            return response()->json("Comment deleted", 200);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    /**
     * Delete post
     * @param DestroyCommentRequest
     * @return Response
     */
    public function deletePost (DestroyPostRequest $request, $postId) {
        $userId = $request->user->id;
        $user = $this->getUserWithPostId($userId, $postId);
        try {
            $instagram = new Instagram($user->username, $user->password);
            $instagram->deletePost($user->post_id);

            \App\Models\Post::find($postId)->delete();

            return response()->json("Post deleted", 200);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get user with post id
     * @param $userId, $posId
     * @return Response
     */
    private function getUserWithPostId($userId, $postId) {
        return \DB::table('users')
            ->join('agencies', 'users.id', '=', 'agencies.user_id')
            ->join('brands', 'brands.agency_id', '=', 'agencies.id')
            ->join('social_media_accounts', 'social_media_accounts.brand_id', '=', 'brands.id')
            ->join('posts', 'posts.social_media_account_id', '=', 'social_media_accounts.id')
            ->where('user_id', $userId)
            ->where('posts.id', $postId)
            ->select('social_media_accounts.username', 'social_media_accounts.password', 'posts.post_id')
            ->first();
    }
}
