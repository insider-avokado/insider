<?php

namespace App\Http\Controllers;

use App\Repositories\SocialAccountRepository;
use Illuminate\Http\Request;
use App\Http\Requests\SocialAccountRequest;
use App\Repositories\BrandRepository;

class SocialAccountsController extends Controller
{

    /**
     * Get list of user brands
     * @params $request
     * @return Response
     */
    public function get(Request $request, $brandId)
    {
        $socialRepo = new SocialAccountRepository();
        $list = $socialRepo->getList($brandId);
        return response()->json($list, 200);
    }

    /**
     * Create new brand
     * @params $request
     * @return Response
     */
    public function create(SocialAccountRequest $request)
    {
        $hash = $request->input('hash');
        $username = $request->input('username');
        $password = $request->input('password');
        $accountType = $request->input('account_type');

        $socialRepo = new SocialAccountRepository();
        $brandRepo = new BrandRepository();
        $checkHash = $brandRepo->getByHash($hash);
        if(!$checkHash){
            return response('Not Found', 404);
        }
        $brandId = $checkHash->id;
        $create = $socialRepo->create([
            'username' => $username,
            'password' => $password,
            'brand_id' => $brandId,
            'account_type' => $accountType,
        ]);
        if ($create) {
            return response('Success', 200);
        }
        return response('Error in creating new Social Media Account, please try again later', 500);
    }
}
