<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribeRequest;
use App\Repositories\SubscriptionRepository;
use Illuminate\Http\Request;

class SubscriptionsController extends Controller
{
    /**
     * get the current plan of the user subscription
     * @param Request
     * @return JSONResponse
     */
    public function get(Request $request)
    {
        $userId = $request->user->id;
        $subRepo = new SubscriptionRepository();
        $subscription = $subRepo->get($userId);
        if (!$subscription) {
            $subscribe = $subRepo->subscribe($userId, 'free');
            return response()->json($subscribe);
        }
        return response()->json($subscription);
    }

    /**
     * Subscribe user to a plan
     * @param SubscribeRequest
     * @return JSONResponse
     */
    public function subscribe(SubscribeRequest $request)
    {
        $plan = $request->input('plan');
        $userId = $request->user->id;
        $subRepo = new SubscriptionRepository();
        $subscribe = $subRepo->subscribe($userId, $plan);
        return response()->json($subscribe);

    }
}
