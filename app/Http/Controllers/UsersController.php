<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Repositories\AgencyRepository;
use App\Repositories\UserRepository;

class UsersController extends Controller
{

    /**
     * Update user data
     * @param UserRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UserRequest $request, int $id)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $name = $request->input('name');
        $email = $request->input('email');
        $userRepo = new UserRepository();
        $agencyRepo = new AgencyRepository();

        $user = $userRepo->getOne($id);

        if (!$user) {
            return response('Not Found', 404);
        }
        $update = $userRepo->update($id, [
            'name' => $name,
            'email' => $email,
            'password' => \Hash::make($password),
            'username' => $username,
        ]);

        if (!$update) {
            return response('Cannot update the user data, please try again later', 500);
        }

        $agencyRepo->update($user->agency->id, [
            'name' => $name,
        ]);

        return response('Success', 200);
    }
}
