<?php

namespace App\Http\Middleware;

use App\Helpers\Jwt;
use App\Repositories\UserRepository;
use Closure;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $jwt = new Jwt();
        $token = $request->header('Authorization');
        if (!$token) {
            return response()->json('Unauthorized', 401);
        }
        $token = explode(' ', $request->header('Authorization'));
        if ($token[0] == 'Bearer') {
            if (isset($token[1])) {
                $token = $jwt->check($token[1]);
                if ($token[0] == 'success') {
                    $userRepository = new UserRepository();
                    $user = $userRepository->getOne(intval($token[1]->id));
                    $request->merge([
                        'user' => $user,
                    ]);

                    return $next($request);
                }
            }
        }
        return response()->json('Unauthorized', 401);
    }
}
