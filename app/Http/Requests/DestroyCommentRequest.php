<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DestroyCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $userId = request()->user->id;

        return \DB::table('users')
            ->join('agencies', 'users.id', '=', 'agencies.user_id')
            ->join('brands', 'brands.agency_id', '=', 'agencies.id')
            ->join('social_media_accounts', 'social_media_accounts.brand_id', '=', 'brands.id')
            ->join('posts', 'posts.social_media_account_id', '=', 'social_media_accounts.id')
            ->where('user_id', $userId)
            ->select('social_media_accounts.username', 'social_media_accounts.password')
            ->first();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
