<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'username' => ['required', 'min:4', 'max:30', Rule::unique('users')],
            'name' => 'required|min:4|max:100|regex:/^[\p{Arabic}\pL\d ]+$/u',
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => 'required|min:4|max:30',
        ];
        if ($this->method() == 'PATCH') {
            $rules['username'] = ['required', 'min:4', 'max:30', Rule::unique('users')->ignore(request()->route('id'))];
            $rules['email'] = ['required', 'email', Rule::unique('users')->ignore(request()->route('id'))];
        }

        return $rules;
    }

}
