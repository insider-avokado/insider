<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    protected $table = 'agencies';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brands()
    {
        return $this->hasMany(Brand::class, 'agency_id');
    }
}
