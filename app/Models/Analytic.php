<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{

    protected $table = 'analytics';

    protected $guarded = ['id'];
}
