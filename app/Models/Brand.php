<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    protected $table = 'brands';

    protected $guarded = ['id'];

    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agency_id');
    }

    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class, 'brand_id');
    }
}
