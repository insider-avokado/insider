<?php

namespace App\Repositories;

use App\Models\Agency;
use App\Repositories\Repository;

class AgencyRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Agency';
    }

    public function create(array $data): ?Agency
    {
        try {
            return $this->model->create($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function update(int $id, array $data): ?bool
    {
        try {
            return $this->model->where('id', $id)->update($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function getList(): array
    {
        try {
            return $this->model->pluck('name', 'id')->toArray();
        } catch (\PDOException $e) {
            return [];
        }
    }
}
