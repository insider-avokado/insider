<?php

namespace App\Repositories;

use App\Repositories\Repository;

class AnalyticRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Analytic';
    }

    public function getList($postId = null)
    {
        return $this->model->where(function ($query) use ($postId) {
            if ($postId != null) {
                $query->where('post_id', $postId);
            }
        })->get(['numberOfDays', 'likedCount', 'reachedPerson', 'commentNumber']);

    }
}
