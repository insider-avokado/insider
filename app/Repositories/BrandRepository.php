<?php

namespace App\Repositories;

use App\Models\Brand;
use App\Repositories\Repository;

class BrandRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Brand';
    }

    public function getList($agencyId = null)
    {
        return $this->model->
            with(['socialAccounts' => function($query){
                $query->select(['id','brand_id','account_type']);
            }])
            ->where(function ($query) use ($agencyId) {
                if ($agencyId != null) {
                    $query->where('agency_id', $agencyId);
                }
            })->get();

    }

    public function create(array $data): ?Brand
    {
        try {
            return $this->model->create($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function getByHash(string $hash): ?Brand
    {
        try {
            return $this->model->where('hash', $hash)->first();
        } catch (\PDOException $e) {
            return null;
        }
    }
}
