<?php

namespace App\Repositories;

abstract class Repository
{

    protected $model;

    abstract public function model();

    public function __construct()
    {
        $this->model = app()->make($this->model());
    }

    public function getOne(int $id)
    {
        try {
            return $this->model->where('id', $id)->first();
        } catch (\PDOException $e) {
            return null;
        }
    }
}
