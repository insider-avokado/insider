<?php

namespace App\Repositories;

use App\Models\SocialAccount;
use App\Repositories\Repository;

class SocialAccountRepository extends Repository
{

    public function model()
    {
        return 'App\Models\SocialAccount';
    }

    public function create(array $data): ?SocialAccount
    {
        try {
            return $this->model->create($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function update(int $id, array $data): ?bool
    {
        try {
            return $this->model->where('id', $id)->update($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function getList(int $brandId): array
    {
        try {
            return $this->model
                ->where('brand_id', $brandId)
                ->pluck('account_type', 'id')
                ->toArray();
        } catch (\PDOException $e) {
            return [];
        }
    }

}
