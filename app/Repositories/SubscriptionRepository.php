<?php

namespace App\Repositories;

use App\Models\Brand;
use App\Repositories\Repository;
use App\Models\Subscription;

class SubscriptionRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Subscription';
    }

    public function get(int $userId)
    {
        try {
            return $this->model->where('user_id', $userId)->first();
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function subscribe(int $userId, string $plan): ?Subscription
    {
        try {
            $subscribe = $this->model->where('user_id', $userId)->first();
            if ($subscribe) {
                $subscribe->subscription_type = $plan;
                $subscribe->save();
            } else {
                $subscribe = $this->model->create([
                    'user_id' => $userId,
                    'subscription_type' => $plan,
                ]);
            }
            return $subscribe;
        } catch (\PDOException $e) {
            return null;
        }
    }

}
