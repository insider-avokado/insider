<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\User;

class UserRepository extends Repository
{

    public function model()
    {
        return 'App\User';
    }

    public function create(array $data): ?User
    {
        try {
            return $this->model->create($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function update(int $id, array $data): ?bool
    {
        try {
            return $this->model->where('id', $id)->update($data);
        } catch (\PDOException $e) {
            return null;
        }
    }

    public function delete(int $id): bool
    {
        try {
            return $this->model->where('id', $id)->delete();
        } catch (\PDOException $e) {
            if ($e->getCode() == 23000) {
                return false;
            }
            return false;
        }
    }
}
