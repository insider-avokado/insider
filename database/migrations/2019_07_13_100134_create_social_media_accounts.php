<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialMediaAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_media_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('account_type', ['facebook', 'twitter', 'instagram']);
            $table->bigInteger('brand_id')->unsigned();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('access_token')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('social_media_accounts');
        Schema::enableForeignKeyConstraints();
    }
}
