<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('social_media_account_id')->unsigned();
            $table->text('post_body')->nullable();
            $table->string('photo_filename')->nullable();
            $table->datetime('time_to_post')->nullable();
            $table->enum('status', ['new', 'failed', 'success'])->default('new');
            $table->timestamps();
            $table->string('post_id')->nullable();
            $table->string('post_url')->nullable();
            $table->foreign('social_media_account_id')->references('id')->on('social_media_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('posts');
        Schema::enableForeignKeyConstraints();
    }
}
