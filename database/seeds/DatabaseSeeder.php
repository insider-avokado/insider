<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('tr_TR');
        //create Super admin user
        \DB::table('users')->insert([
            'name' => 'I"m Super User',
            'username' => 'super.user',
            'email' => 'super@user.com',
            'password' => \Hash::make('123123'),
            'user_type' => 'super',
        ]);

        $agencyUsername = 'agency';
        //create agencies and thier brands with social accounts
        for ($i = 0; $i < 10; $i++) {
            //create user
            $userId = \DB::table('users')->insertGetId([
                'name' => $faker->name,
                'username' => $i == 1?$agencyUsername:$faker->userName,
                'email' => $faker->companyEmail,
                'password' => \Hash::make('123123'),
            ]);

            //create agency

            $agencyId = \DB::table('agencies')->insertGetId([
                'user_id' => $userId,
                'name' => $faker->company,
                'avatar' => $faker->image(null, 500, 500),
            ]);

            //create brands

            for ($j = 0; $j < 3; $j++) {
                $email = $faker->email;
                $brandId = \DB::table('brands')->insertGetId([
                    'agency_id' => $agencyId,
                    'name' => $faker->name,
                    'email' => $email,
                    'hash' => \Hash::make($email),
                ]);

                //create social accounts
                $accountId = \DB::table('social_media_accounts')->insertGetId([
                    'brand_id' => $brandId,
                    'account_type' => ['facebook', 'instagram', 'twitter'][rand(0, 2)],

                ]);
                for ($k = 0; $k < 4; $k++) {
                    $postId = \DB::table('posts')->insertGetId([
                        'social_media_account_id' => $accountId,
                        'post_body' => $faker->realText(50),
                        'post_url' => '',
                        'photo_filename' => 'logo-avocado.png',
                        'time_to_post' => null,
                    ]);

                    for ($d = 1; $d < 8; $d++) {
                        $analyticId = \DB::table('analytics')->insertGetId([
                            'post_id' => $postId,
                            'numberOfDays' => $d,
                            'likedCount' => rand(1000, 10000),
                            'reachedPerson' => rand(1000, 10000),
                            'commentNumber' => rand(1000, 10000)
                        ]);
                    }
                }
            }

            \DB::table('social_media_accounts')
                ->where('id', 1)
                ->update(['username' => 'insider.avokado', 'password' => 'Avokadoo123']);

            \DB::table('posts')
                ->where('id', 1)
                ->update(['post_id' => '2087146905713033881_16236588867']);

            \DB::table('agencies')
                ->where('id', 1)
                ->update(['user_id' => 1]);
        }
    }
}
