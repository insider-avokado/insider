@component('mail::message')
Hello {{$brand->name}},
Please, click on the button to open a form, so you can add your social media accounts

@component('mail::button', ['url' => $brand->form_url, 'color' => 'primary'])
View Form
@endcomponent
Sincerely,
Avokado Team.
@endcomponent
