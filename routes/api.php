<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', ['uses' => 'Auth\LoginController@login']);
    Route::post('register', ['uses' => 'Auth\RegisterController@register']);
    Route::group(['prefix' => '', 'as' => '', 'middleware' => ['jwt.check']], function () {
        Route::group(['prefix' => 'users', 'as' => 'users'], function () {
            Route::patch('{id}', ['as' => 'update', 'uses' => 'UsersController@update']);
        });
        Route::group(['prefix' => 'subscriptions', 'as' => 'subscriptions'], function () {
            Route::get('', ['as' => 'get', 'uses' => 'SubscriptionsController@get']);
            Route::post('', ['as' => 'subscribe', 'uses' => 'SubscriptionsController@subscribe']);

        });
        Route::group(['prefix' => 'brands', 'as' => 'brands'], function () {
            Route::get('', ['as' => 'get', 'uses' => 'BrandsController@get']);
            Route::post('', ['as' => 'create', 'uses' => 'BrandsController@create']);
            Route::group(['prefix' => '{brandId}/accounts', 'as' => 'accounts'], function () {
                Route::get('', ['as' => 'list', 'uses' => 'SocialAccountsController@get']);
            });
        });

        Route::group(['prefix' => 'posts', 'as' => 'posts'], function () {
            Route::get('/account-id/{id}', 'PostsController@posts');
            Route::get('{id}', ['as' => 'get-one', 'uses' => 'PostsController@getOne']);
            Route::post('post-media', 'PostsController@postMedia');
            Route::post('write-comment', 'PostsController@writeComment');
            Route::get('comments/{id}', 'PostsController@getComments');
            Route::delete('{id}', 'PostsController@deletePost');
            Route::delete('/comments/{postId}/{commentId}', 'PostsController@deleteComment');
        });

        Route::group(['prefix' => 'analytics', 'as' => 'analytics'], function () {
            Route::get('{id?}', ['as' => 'get', 'uses' => 'AnalyticsController@get']);
        });
    });
    Route::post('brands/checkHash', ['as' => 'brands/check-hash', 'uses' => 'BrandsController@checkHash']);
    Route::post('brands/accounts', ['as' => 'create', 'uses' => 'SocialAccountsController@create']);
});
